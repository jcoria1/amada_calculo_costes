module amada.oxiebro.oxiebro {
    requires javafx.controls;
    requires javafx.fxml;


    opens amada.oxiebro.oxiebro to javafx.fxml;
    exports amada.oxiebro.oxiebro;
}