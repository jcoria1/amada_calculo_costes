# Index
## Obtención de datos
### Datos obtenibles XML (CUTTING_CONDITION.XML)
Este archivo XML se obtiene de manera automática al tener el programa AMADA3i
```sql
    -- Ruta de archivo de ejemplo
    C:\ProgramData\AMADA3i\ParameterExplorer_Base\localhost\Machines\Blank\ENSIS3015AJ-9KW
```
    - MaterialType
    - Thickness
    - NameCutCondition
    - Speed
    - Power (Output)
    - Frequency
    - Duty
    - Kind (tipo de gas)
    - Pressure
    - OrigionRepos (distancia de boquilla)
    - Diameter
    - ApproachData
    - EdgeData
    - Focus
    - Pulse 
    - SwitchTime
    - HeadControl
    - Pierce
    - CutMonitor

## Proyecto Java

### Edición de archivo XLSX

#### Formato de celdas

|Celda|Formato|
|---|---|
|Cutting Speed|NUMERIC|
|Power|NUMERIC|
|Duty|NUMERIC|
|Assist gas|STRING|
|Nozzle|STRING|
|Gas Pressure|NUMERIC|
|Nozzle Gap|NUMERIC|
|Process time|STRING|
|Electrical cost|FORMULA|
|Assist Gas cost|FORMULA|
|Total|FORMULA|
#### Datos de interés

- La hoja de cálculo ejecuta las funciones aún y estando cerrada, permitiendo así obtener los cálculos de costes
- Por el momento, si la hoja de cálculo está abierta no se puede ejecutar el código (investigar)
- El proyecto recibe unos valores y te devuelve los cálculos

#### Hoja de ruta

Crear archivo .exe temporal para el uso interno y tests, y comenzar a trabajar en el diseño de la aplicación usando javaFX

### Archivo CSV

#### Formato de archivo

![imgcsv](res/configuracionCSV.PNG)
