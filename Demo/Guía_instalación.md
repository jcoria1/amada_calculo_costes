# Guía de instalación 
En esta guía veremos como utilizar correctamente el software de Amada para el cálculo de costes de máquinas de corte por láser.

## Pre Requisitos

### Java18 (JDK)

Para poder utilizar el software es necesario tener instalado la versión 18 de Java(JDK), disponible desde la página web de oracle, o bien desde este link:

[Link de descarga para windows](https://download.oracle.com/java/18/archive/jdk-18_windows-x64_bin.exe)

[Página web de Oracle](https://www.oracle.com/java/technologies/javase/jdk18-archive-downloads.html)

Una vez descargado (se recomienda utilizar el primer link), vamos a continuar con la instalación de Java18(JDK).

`Haremos doble click en el archivo que se nos ha descargado, y nos aparecerá la siguiente ventana: `

![jdk1](res/jdk1.PNG)

`Hacemos click en next, y nos aparecerá la siguiente ventana (continuaremos pulsando 'Next'):`

![jdk2](res/jdk2.PNG)

`Después nos saldrá una ventana con una barra de progreso, esperaremos a que se instale el programa.`

![jdk3](res/jdk3.PNG)

`Finalmente, el programa estará instalado y le daremos a 'Close'`

![jdk4](res/jdk4.PNG)

Ahora tenemos Java18 instalado, y podemos continuar con los prerequisitos.

### Carpetas y archivos

Para que el programa funcione como se espera, es necesario tener una carpeta con los archivos .xlsx para el control de costes. 

La carpeta se llama costes y estará en la siguiente ruta:

`C:\Amada Config\costes`

Podemos descargarla desde el siguiente link:

[Link de descarga de la carpeta](https://drive.google.com/drive/folders/1Auu6wPJ9CtrfWNXNl9Y4F-zeZMTOCdi-?usp=sharing)

Dentro de esa carpeta estarán todos los archivos necesarios, incluyendo el archivo .exe para hacer los cálculos de manera automática (el .exe se puede mover a cualquier ruta, para mayor conveniencia, o utilizar un acceso directo)

Una vez realizado esto, el programa está listo para usarse

### Configuración CSV

Es necesario utilizar una configuración concreta de exportación de los archivos CSV para poder usar el software. Esta configuración se aplica en el programa de Amada 'ABE PLANNER'

Imagen de la configuración: 

![imgcsv](res/configuracionCSV.PNG)

## Uso del programa

Para utilizar el programa es necesario obtener los archivos CSV fruto de generar el nesting, una vez los tenemos simplemente ejecutamos el programa (dependiendo de si tienes las máquinas en la ruta server o localhost, elegiremos Calculo_costes_server.exe o bien Calculo_costes.exe), arrastramos el archivo CSV hasta la terminal y le damos a enter. Una vez hecho el programa realizará los cálculos pertinentes y nos devolverá por pantalla los cálculos de costes que queremos.

![imgcc1](res/cc1.PNG)

![imgcc2](res/cc2.PNG)

![imgcc3](res/cc3.PNG)
