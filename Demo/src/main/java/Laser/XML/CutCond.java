package Laser.XML;


import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlElementWrapper;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;
public class CutCond {

    @JacksonXmlProperty(isAttribute = true, localName = "NozzleType")
    private String NozzleType;
    @JacksonXmlProperty(isAttribute = true, localName = "NozzleDia")
    private String NozzleDia;
    @JacksonXmlProperty(localName = "CutProcessCond")
    private CutProcessCond cutProcessCond;

    public CutCond() {
        super();
    }

    public void setNozzleType(String nozzleType) {
        NozzleType = nozzleType;
    }

    public void setNozzleDia(String nozzleDia) {
        NozzleDia = nozzleDia;
    }

    public void setCutProcessCond(CutProcessCond cutProcessCond) {
        this.cutProcessCond = cutProcessCond;
    }

    public CutProcessCond getCutProcessCond() {
        return cutProcessCond;
    }

    public String getNozzleType() {
        return NozzleType;
    }

    public double getNozzleDia() {
        return Double.parseDouble(NozzleDia);
    }

    public CutCond(String nozzleType,
                   String nozzleDia,
                   CutProcessCond cutProcessCond) {
        this.NozzleType = nozzleType;
        this.NozzleDia = nozzleDia;
        this.cutProcessCond = cutProcessCond;
    }
    public String getNozzle(){
        switch(getNozzleType()){
            case "0"-> {return "S"+getNozzleDia();}
            case "1"-> {return "D"+getNozzleDia()+"W";}
            case "2"-> {return "S"+getNozzleDia()+"E";}
            case "3"-> {return "D"+getNozzleDia()+"E";}
            case "4"-> {return "D"+getNozzleDia();}
            case "5"-> {return "S"+getNozzleDia()+"C";}
            case "6"-> {return "D"+getNozzleDia()+"C";}
            case "7"-> {return "D"+getNozzleDia()+"SC";}
            case "8"-> {return "S"+getNozzleDia()+"FE";}
            case "9"-> {return "D"+getNozzleDia()+"FE";}
            case "10"-> {return "D"+getNozzleDia()+"AL";}
            case "11"-> {return "D"+getNozzleDia()+"F";}
            case "12"-> {return "D"+getNozzleDia()+"FW";}
            default -> {
                return "Something went Wrong";
            }
        }
    }
}
