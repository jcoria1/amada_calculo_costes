package Laser.XML;



import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlElementWrapper;

import java.util.List;

public class CutProcessCond {
    @JacksonXmlElementWrapper(useWrapping = false)
    private List<ProcessC> ProcessC;

    public void setProcessCList(List<ProcessC> ProcessC) {
        this.ProcessC = ProcessC;
    }

    public CutProcessCond() {
        super();
    }

    public List<ProcessC> getProcessCList() {
        return ProcessC;
    }

    public CutProcessCond( List<ProcessC> ProcessC) {
        this.ProcessC = ProcessC;
    }
}
