module com.example.demo1 {
    requires javafx.controls;
    requires javafx.fxml;
    requires javafx.web;

    requires org.controlsfx.controls;
    requires com.dlsc.formsfx;
    requires validatorfx;
    requires org.kordamp.ikonli.javafx;
    requires org.kordamp.bootstrapfx.core;
    requires eu.hansolo.tilesfx;
    requires com.fasterxml.jackson.databind;
    requires com.fasterxml.jackson.dataformat.xml;
    requires com.opencsv;
    requires org.apache.poi.poi;
    requires org.apache.poi.ooxml;
    requires java.sql;
    opens com.example.demo1;
    exports com.example.demo1 to javafx.fxml, javafx.graphics;
    opens com.example.DatosCSV to com.fasterxml.jackson.databind;
    opens com.example.XML to com.fasterxml.jackson.databind;

    exports com.example.DatosCSV;
    exports com.example.Resultados;
    exports com.example.XML;
}