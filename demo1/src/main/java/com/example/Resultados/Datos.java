package com.example.Resultados;

public class Datos {
    private String name;
    private String nombre_cutting_cond;
    private String nombre_material_laser;
    private String speed;
    private String power;
    private String duty;
    private String assist_gas;
    private String nozzle;
    private String gas_pressure;
    private String nozzle_gap;
    private String process_time;

    public Datos(String name,String nombre_cutting_cond, String nombre_material_laser, String speed, String power, String duty, String assist_gas, String nozzle, String gas_pressure, String nozzle_gap, String process_time) {
        this.name = name;
        this.nombre_cutting_cond = nombre_cutting_cond;
        this.nombre_material_laser = nombre_material_laser;
        this.speed = speed;
        this.power = power;
        this.duty = duty;
        this.assist_gas = assist_gas;
        this.nozzle = nozzle;
        this.gas_pressure = gas_pressure;
        this.nozzle_gap = nozzle_gap;
        this.process_time = process_time;
    }

    public String getName() {
        return name;
    }

    public String getNombre_cutting_cond() {
        return nombre_cutting_cond;
    }

    public void setNombre_cutting_cond(String nombre_cutting_cond) {
        this.nombre_cutting_cond = nombre_cutting_cond;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getNombre_material_laser() {
        return nombre_material_laser;
    }

    public void setNombre_material_laser(String nombre_material_laser) {
        this.nombre_material_laser = nombre_material_laser;
    }

    public String getSpeed() {
        return speed;
    }

    public void setSpeed(String speed) {
        this.speed = speed;
    }

    public int getPower() {
        if (Integer.parseInt(power)==9999){
            power = "12000";
        }
        return Integer.parseInt(power);
    }

    public void setPower(String power) {
        this.power = power;
    }

    public int getDuty() {
        return Integer.parseInt(duty);
    }

    public void setDuty(String duty) {
        this.duty = duty;
    }

    public String getAssist_gas() {
        return assist_gas;
    }

    public void setAssist_gas(String assist_gas) {
        this.assist_gas = assist_gas;
    }

    public String getNozzle() {
        return nozzle;
    }

    public void setNozzle(String nozzle) {
        this.nozzle = nozzle;
    }

    public double getGas_pressure() {
        return Double.parseDouble(gas_pressure);
    }

    public void setGas_pressure(String gas_pressure) {
        this.gas_pressure = gas_pressure;
    }

    public double getNozzle_gap() {
        return Double.parseDouble(nozzle_gap);
    }

    public void setNozzle_gap(String nozzle_gap) {
        this.nozzle_gap = nozzle_gap;
    }

    public String getProcess_time() {
        return process_time;
    }

    public void setProcess_time(String process_time) {
        this.process_time = process_time;
    }

    public Datos() {
    }
}
