package com.example.Resultados;

public class Costs {
    public String name;



    public double electrical_costs;
    public double gas_cost;
    public double total;
    public String time;

    @Override
    public String toString() {
        return  "\nNombre: " + name +
                "\n\tTime(hh:mm:ss): "+time+
                "\n\tElectrical cost (€): " + electrical_costs +
                "\n\tAssist Gas cost (€): " + gas_cost +
                "\n\tTotal (€): " + total;
    }

    public Costs(String name, double electrical_costs, double gas_cost, double total, String time) {
        this.name = name;
        this.electrical_costs = electrical_costs;
        this.gas_cost = gas_cost;
        this.total = total;
        this.time = time;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public double getElectrical_costs() {
        return electrical_costs;
    }

    public void setElectrical_costs(double electrical_costs) {
        this.electrical_costs = electrical_costs;
    }

    public double getGas_cost() {
        return gas_cost;
    }

    public void setGas_cost(double gas_cost) {
        this.gas_cost = gas_cost;
    }

    public double getTotal() {
        return total;
    }

    public void setTotal(double total) {
        this.total = total;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
    public Costs() {
    }

}
