# Index
## Obtención de datos
### Datos obtenibles XML (CUTTING_CONDITION.XML)
Este archivo XML se obtiene de manera automática al tener el programa AMADA3i
```sql
    -- Ruta de archivo de ejemplo
    C:\ProgramData\AMADA3i\ParameterExplorer_Base\localhost\Machines\Blank\ENSIS3015AJ-9KW
```
    - MaterialType
    - Thickness
    - NameCutCondition
    - Speed
    - Power (Output)
    - Frequency
    - Duty
    - Kind (tipo de gas)
    - Pressure
    - OrigionRepos (distancia de boquilla)
    - Diameter
    - ApproachData
    - EdgeData
    - Focus
    - Pulse 
    - SwitchTime
    - HeadControl
    - Pierce
    - CutMonitor

## Proyecto Java

### Edición de archivo XLSX

#### Formato de celdas

|Celda|Formato|
|---|---|
|Cutting Speed|NUMERIC|
|Power|NUMERIC|
|Duty|NUMERIC|
|Assist gas|STRING|
|Nozzle|STRING|
|Gas Pressure|NUMERIC|
|Nozzle Gap|NUMERIC|
|Process time|STRING|
|Electrical cost|FORMULA|
|Assist Gas cost|FORMULA|
|Total|FORMULA|
#### Datos de interés

- La hoja de cálculo ejecuta las funciones aún y estando cerrada, permitiendo así obtener los cálculos de costes
- Por el momento, si la hoja de cálculo está abierta no se puede ejecutar el código (investigar)
- El proyecto recibe unos valores y te devuelve los cálculos

#### Hoja de ruta

### Archivo CSV

#### Formato de archivo

![imgcsv](res/configuracionCSV.PNG)

Estoy valorando el incluir el nombre de la chapa dentro de cada pieza, para poder relacionar piezas con chapas

**POSIBLE CONFIGURACIÓN (no definitiva)**

![imgcsv2](res/configuracionCSV2.PNG)

#### Movidas del archivo

Ejemplo:

![Test](res/csv.PNG)

Como se puede ver, el csv te indica cuantas piezas se han hecho en x chapa, pero no te hace el cálculo total, teniendo así casos como este ejemplo, en el que tenemos un nesting que han sido necesarias dos chapas y las piezas se han repartido entre ellas, por lo tanto tenemos dos "soporte-pergola" y dos "soporteaslul", que en realidad son lo mismo, pero están separados entre dos chapas. Valorar si en el programa deberían tratarse como dos objetos diferentes o bien sumarlos (yo creo que lo suyo es sumarlos, pero habrá que hablarlo) para obtener los cálculos.

También tengo que ver si merece la pena realmente sumarlos, o tratarlos como individuales y en ese caso relacionar piezas con chapas, en este caso separar los objetos en el momento de recibir la lista del csv y poner chapa -> list(piezas)

