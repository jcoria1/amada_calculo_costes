package XML;

import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlElementWrapper;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlRootElement;

import java.util.List;
@JacksonXmlRootElement(localName = "ArrayOfVLaserMaterial")
public class ArrayOfVLaserMaterial{
    @JacksonXmlElementWrapper( useWrapping = false)
    private List<vLaserMaterial> vLaserMaterial;

    public List<vLaserMaterial> getvLaserMaterialEntries() {
        return vLaserMaterial;
    }

    public ArrayOfVLaserMaterial(List<vLaserMaterial> content) {
        this.vLaserMaterial = content;
    }

    public void setvLaserMaterialEntries(List<vLaserMaterial> vLaserMaterialEntries) {
        this.vLaserMaterial = vLaserMaterialEntries;
    }

    public ArrayOfVLaserMaterial() {
        super();
    }
}
