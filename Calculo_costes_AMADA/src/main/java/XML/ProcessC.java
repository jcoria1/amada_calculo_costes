package XML;


import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;

public class ProcessC {

    @JacksonXmlProperty(isAttribute = true, localName = "Name")
    private String Name;
    @JacksonXmlProperty(isAttribute = true, localName = "Speed")
    private String Speed;
    @JacksonXmlProperty(isAttribute = true, localName = "Output")
    private String Output;
    @JacksonXmlProperty(isAttribute = true, localName = "Duty")
    private String Duty;
    @JacksonXmlProperty(isAttribute = true, localName = "Kind")
    private String Kind;
    @JacksonXmlProperty(isAttribute = true, localName = "Pressure")
    private String Pressure;
    @JacksonXmlProperty(isAttribute = true, localName = "OrigionRepos")
    private String OrigionRepos;

    public void setName(String name) {
        Name = name;
    }

    public void setSpeed(String speed) {
        Speed = speed;
    }

    public void setOutput(String output) {
        Output = output;
    }

    public void setDuty(String duty) {
        Duty = duty;
    }

    public void setKind(String kind) {
        Kind = kind;
    }

    public void setPressure(String pressure) {
        Pressure = pressure;
    }

    public void setOrigionRepos(String origionRepos) {
        OrigionRepos = origionRepos;
    }

    public String getName() {
        return Name;
    }

    public String getSpeed() {
        return Speed;
    }

    public String getOutput() {
        return Output;
    }

    public String getDuty() {
        return Duty;
    }

    public String getKind() {
        switch (Kind) {
            case "1", "3", "2" -> {
                return "Oxygen";
            }
            case "4" -> {
                return "Nitrogen";
            }
            case "5" ->{
                return "";
            }
            default -> {
                System.out.println("ERROR, KIND: "+Kind);
                return "Error";
            }
        }
    }

    public String getPressure() {
        return Pressure;
    }

    public String getOrigionRepos() {
        return OrigionRepos;
    }

    public ProcessC() {super();

    }

    public ProcessC(String name, String speed, String output, String duty, String kind, String pressure, String origionRepos) {
        Name = name;
        Speed = speed;
        Output = output;
        Duty = duty;
        Kind = kind;
        Pressure = pressure;
        OrigionRepos = origionRepos;
    }
}
