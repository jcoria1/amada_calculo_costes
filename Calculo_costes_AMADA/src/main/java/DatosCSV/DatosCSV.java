package DatosCSV;

import com.opencsv.bean.CsvBindByPosition;

public class DatosCSV {
    @Override
    public String toString() {
        return "DatosCSV{" +
                "tipo='" + tipo + '\'' +
                ", nombre='" + nombre + '\'' +
                ", cantidad='" + cantidad + '\'' +
                ", nombre_maquina='" + nombre_maquina + '\'' +
                ", tiempo='" + tiempo + '\'' +
                ", nombre_material_laser='" + nombre_material_laser + '\'' +
                ", nombre_material='" + nombre_material + '\'' +
                ", grosor='" + grosor + '\'' +
                ", sizeX='" + sizeX + '\'' +
                ", sizeY='" + sizeY + '\'' +
                '}';
    }

    @CsvBindByPosition(position = 0)
    private String tipo;

    @CsvBindByPosition(position = 1)
    private String nombre;

    @CsvBindByPosition(position = 2)
    private String cantidad;

    @CsvBindByPosition(position = 3)
    private String nombre_maquina;

    @CsvBindByPosition(position = 4)
    private String tiempo;

    @CsvBindByPosition(position = 5)
    private String nombre_material_laser;

    @CsvBindByPosition(position = 6)
    private String nombre_material;

    @CsvBindByPosition(position = 7)
    private String grosor;

    @CsvBindByPosition(position = 8)
    private String sizeX;

    @CsvBindByPosition(position = 9)
    private String sizeY;

    public DatosCSV() {
    }

    public DatosCSV(String tipo, String nombre, String cantidad, String nombre_maquina, String tiempo, String nombre_material_laser, String nombre_material, String grosor, String sizeX, String sizeY) {
        this.tipo = tipo;
        this.nombre = nombre;
        this.cantidad = cantidad;
        this.nombre_maquina = nombre_maquina;
        this.tiempo = tiempo;
        this.nombre_material_laser = nombre_material_laser;
        this.nombre_material = nombre_material;
        this.grosor = grosor;
        this.sizeX = sizeX;
        this.sizeY = sizeY;
    }

    public String getTipo() {
        return tipo;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getCantidad() {
        return cantidad;
    }

    public void setCantidad(String cantidad) {
        this.cantidad = cantidad;
    }

    public String getNombre_maquina() {
        return nombre_maquina;
    }

    public void setNombre_maquina(String nombre_maquina) {
        this.nombre_maquina = nombre_maquina;
    }

    public String getTiempo() {
        return tiempo;
    }

    public void setTiempo(String tiempo) {
        this.tiempo = tiempo;
    }

    public String getNombre_material_laser() {
        return nombre_material_laser;
    }

    public void setNombre_material_laser(String nombre_material_laser) {
        this.nombre_material_laser = nombre_material_laser;
    }

    public String getNombre_material() {
        return nombre_material;
    }

    public void setNombre_material(String nombre_material) {
        this.nombre_material = nombre_material;
    }

    public String getGrosor() {
        return grosor;
    }

    public void setGrosor(String grosor) {
        this.grosor = grosor;
    }

    public String getSizeX() {
        return sizeX;
    }

    public void setSizeX(String sizeX) {
        this.sizeX = sizeX;
    }

    public String getSizeY() {
        return sizeY;
    }

    public void setSizeY(String sizeY) {
        this.sizeY = sizeY;
    }
}
