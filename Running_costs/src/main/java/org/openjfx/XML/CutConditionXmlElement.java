package org.openjfx.XML;


import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;
public class CutConditionXmlElement {
    @JacksonXmlProperty(localName = "CutCond")
    private CutCond cutCond;

    public CutCond getCutCond() {
        return cutCond;
    }

    public void setCutCond(CutCond cutCond) {
        this.cutCond = cutCond;
    }

    public CutConditionXmlElement() {
        super();
    }

    public CutConditionXmlElement(CutCond cutCond) {
        this.cutCond = cutCond;
    }
}
