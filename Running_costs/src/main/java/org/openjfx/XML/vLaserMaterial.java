package org.openjfx.XML;


import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;
public class vLaserMaterial {

    @JacksonXmlProperty(isAttribute = true, localName = "LaserMaterialName")
    private String LaserMaterialName;
    @JacksonXmlProperty(localName = "CutConditionXmlElement")
    private CutConditionXmlElement cutConditionXmlElement;

    public vLaserMaterial(String LaserMaterialName,
                          CutConditionXmlElement cutConditionXmlElement) {
        this.LaserMaterialName = LaserMaterialName;
        this.cutConditionXmlElement = cutConditionXmlElement;
    }

    public vLaserMaterial() {
        super();
    }

    public void setLaserMaterialName(String laserMaterialName) {
        LaserMaterialName = laserMaterialName;
    }

    public void setCutConditionXmlElement(CutConditionXmlElement cutConditionXmlElement) {
        this.cutConditionXmlElement = cutConditionXmlElement;
    }

    public String getLaserMaterialName() {
        return LaserMaterialName;
    }

    public CutConditionXmlElement getCutConditionXmlElement() {
        return cutConditionXmlElement;
    }
}
