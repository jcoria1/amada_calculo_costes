package org.openjfx;

import java.io.*;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.dataformat.xml.XmlMapper;
import com.opencsv.CSVReader;
import com.opencsv.bean.CsvToBeanBuilder;
import javafx.fxml.FXML;
import javafx.scene.control.Label;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.input.DragEvent;
import javafx.scene.input.Dragboard;
import javafx.scene.input.TransferMode;
import javafx.stage.FileChooser;
import org.apache.poi.ss.usermodel.*;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.openjfx.DatosCSV.DatosCSV;
import org.openjfx.Resultados.Costs;
import org.openjfx.Resultados.Datos;
import org.openjfx.XML.ArrayOfVLaserMaterial;
import org.openjfx.XML.CutProcessCond;
import org.openjfx.XML.ProcessC;
import org.openjfx.XML.vLaserMaterial;

public class PrimaryController {
    public static final String processCName = "E005";
    public static FileInputStream fis;
    public static FileOutputStream fos;
    @FXML
    private TextField rutaText;
    @FXML
    private Label welcomeText;
    @FXML
    private TableView<Costs> tableCosts = new TableView<>();
    @FXML
    private TableColumn<Costs, Object> colName = new TableColumn<>("Name");
    @FXML
    private TableColumn<Costs, Object> colTime = new TableColumn<>("Time");

    @FXML
    private TableColumn<Costs, Object> colElectric = new TableColumn<>("Electrical Costs");
    @FXML
    private TableColumn<Costs, Object> colGas = new TableColumn<>("Gas Costs");
    @FXML
    private TableColumn<Costs, Object> colTotal = new TableColumn<>("Total");
    @FXML
    protected void onHelloButtonClick() {
        welcomeText.setText("Welcome to JavaFX Application!");
        final FileChooser fileChooser = new FileChooser();
        fileChooser.getExtensionFilters().addAll(new FileChooser.ExtensionFilter("CSV Files", "*.csv"));
        final File selectedFile = fileChooser.showOpenDialog(welcomeText.getScene().getWindow());
        rutaText.setText(selectedFile.getAbsolutePath());
    }
    @FXML
    protected void onDragDropped(DragEvent event){
        Dragboard db = event.getDragboard();
        boolean success = false;
        if (db.hasFiles()){
            success = true;
            String filepath = db.getFiles().get(0).getAbsolutePath();
            rutaText.setText(filepath);
        }
        event.setDropCompleted(success);
        event.consume();
    }
    @FXML
    protected void onCalcularButtonClick() throws Exception {
        List<Costs> costes = setData(rutaText.getText());
        /*List<Costs> costes = new ArrayList<>();
        costes.add(new Costs("Coste1", 22.02, 2, 72,"0:22:31"));
        costes.add(new Costs("Coste2", 41.72, 544, 1342,"0:12:31"));
        costes.add(new Costs("Coste3", 32.32, 12, 524635,"0:42:31"));
        */
        tableCosts.setOpacity(1);
        colName.setCellValueFactory(new PropertyValueFactory<>("Name"));
        colTime.setCellValueFactory(new PropertyValueFactory<>("Time"));
        colElectric.setCellValueFactory(new PropertyValueFactory<>("electrical_costs"));
        colGas.setCellValueFactory(new PropertyValueFactory<>("gas_cost"));
        colTotal.setCellValueFactory(new PropertyValueFactory<>("Total"));
        tableCosts.setColumnResizePolicy(TableView.CONSTRAINED_RESIZE_POLICY);
        tableCosts.getColumns().clear();
        tableCosts.getColumns().addAll(colName,colTime,colElectric,colGas,colTotal);
        tableCosts.getItems().clear();
        tableCosts.getItems().addAll(costes);
        /*for (Costs costsTmp: costes){
            System.out.println(costsTmp.toString());
        }*/
    }
    @FXML
    protected void onDragOver(DragEvent event){
        Dragboard db = event.getDragboard();
        if (db.hasFiles()){
            event.acceptTransferModes(TransferMode.COPY);
        }else {
            event.consume();
        }
    }
    public static List<Datos> getData(String ruta) throws Exception {
        List<Datos> datosList = new ArrayList<>();
        List<DatosCSV> datosCSV = getCSV(ruta);
        for (DatosCSV datosTmp: datosCSV) {
            XmlMapper xmlMapper = (XmlMapper) new XmlMapper()
                    .configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false)
                    .enable(SerializationFeature.INDENT_OUTPUT);
            //Acabar el Path del archivo cutting_condition.xml

            String path = "C:\\ProgramData\\AMADA3i\\ParameterExplorer_Base\\server\\Machines\\Blank\\"+datosTmp.getNombre_maquina()+"\\CUTTING_CONDITION.xml";
            String readContent = new String(Files.readAllBytes(Paths.get(path)));
            ArrayOfVLaserMaterial example = xmlMapper.readValue(readContent, ArrayOfVLaserMaterial.class);
            vLaserMaterial vLaserMaterial = null;
            for (vLaserMaterial vLaserMaterialTmp : example.getvLaserMaterialEntries()) {
                if (vLaserMaterialTmp.getLaserMaterialName().equals(datosTmp.getNombre_material_laser())) {
                    vLaserMaterial = vLaserMaterialTmp;
                    break;
                }
            }
            //Get ProcessC
            CutProcessCond example2 = vLaserMaterial.getCutConditionXmlElement().getCutCond().getCutProcessCond();
            ProcessC processC = null;
            for (ProcessC processTmp : example2.getProcessCList()) {
                if (processTmp.getName().equals(processCName)) {
                    processC = processTmp;
                    break;
                }
            }


            //Obtenemos Datos
            Datos datoInsert = new Datos();
            //set NAME
            String[] nombre = datosTmp.getNombre_maquina().split("-");
            datoInsert.setName(nombre[0]);
            //set Nombre_material_laser
            datoInsert.setNombre_material_laser(datosTmp.getNombre());
            //set TIMEl
            datoInsert.setProcess_time(datosTmp.getTiempo());
            //get/set Nozzle
            datoInsert.setNozzle(vLaserMaterial.getCutConditionXmlElement().getCutCond().getNozzle());
            //Set nombre_cutting_cond
            datoInsert.setNombre_cutting_cond(datosTmp.getNombre_material_laser());
            //get/set Speed
            datoInsert.setSpeed(processC.getSpeed());

            //get/set Output
            datoInsert.setPower(processC.getOutput());

            //get/set Duty
            datoInsert.setDuty(processC.getDuty());

            //get/set kind
            datoInsert.setAssist_gas(processC.getKind());

            //get/set pressure
            datoInsert.setGas_pressure(processC.getPressure());

            //get/set nozzleGap (origionRepos)
            datoInsert.setNozzle_gap(processC.getOrigionRepos());;

            //Añadimos el objeto en la lista de datos para trabajar en él
            datosList.add(datoInsert);
        }
        /*

        RETURN LIST OF DATA

        */
        return datosList;
    }
    public static List getCSV(String ruta) throws IOException {
        //TESTS
        CSVReader csvReader = new CSVReader(new FileReader(ruta));
        List<DatosCSV> beans = new CsvToBeanBuilder(csvReader)
                .withType(DatosCSV.class).build().parse();
        /*Map<String, List<DatosCSV>> datos = new HashMap<>();
        for (DatosCSV datosTmp:
             beans) {
            if (datosTmp.getTipo().equals("CHAPA")){

            }else{

            }
        }*/
        beans.removeIf(datosCSV -> datosCSV.getTipo().equals("PIEZA"));

        return beans;
        //FIN TESTS
    }
    public static List<Costs> setData(String ruta) throws Exception {

        String filepath = "C:\\Amada Config\\costes";

        List<Datos> datosList = getData(ruta);
        Workbook wb = null;


        List<Costs> costsList= new ArrayList<>();
        for (Datos datosTmp : datosList){
            String[] split = datosTmp.getNombre_cutting_cond().split("-");
            List<Path> files = Files.walk(Path.of(filepath))
                    .filter(Files::isRegularFile)
                    .filter(s -> s.getFileName().toString().startsWith(datosTmp.getName()+"-"+split[split.length-1]))
                    .collect(Collectors.toList());
            /*Iterator<Path> it = files.iterator();
            while (it.hasNext()){
                String[] tmp = it.next().toAbsolutePath().toString().split("-");
                if (Integer.parseInt(tmp[1]) < datosTmp.getPower()){
                    it.remove();
                }
            }
            Collections.sort(files, (p1, p2) -> Integer.parseInt(p1.getFileName().toAbsolutePath().toString().split("-")[1]) - Integer.parseInt(p2.getFileName().toAbsolutePath().toString().split("-")[1]));*/

            fis = new FileInputStream(files.get(0).toAbsolutePath().toString());
            wb = new XSSFWorkbook(fis);
            Sheet sheet = wb.getSheet("Cost simulation"); //Leemos los valores que nos hemos encontrado en la tabla antes de modificarlos y los mostramos por pantalla
            // Ahora vamos a insertar datos de ejemplo en la hoja de cálculo, para después obtener los valores que nos interesan y mostrarlos por pantalla
            for (int k = 16; k<31; k++){
                Row rowInsert = sheet.getRow(k);
                Cell valueName = rowInsert.getCell(1);
                Cell value = rowInsert.getCell(4);
                if(value == null) {
                    value = rowInsert.createCell(4);
                }
                switch (valueName.getStringCellValue()){
                    case "Cutting Speed" -> value.setCellValue(datosTmp.getSpeed());
                    case "Power" -> value.setCellValue(datosTmp.getPower());
                    case "Duty" -> value.setCellValue(datosTmp.getDuty());
                    case "Assist gas" -> value.setCellValue(datosTmp.getAssist_gas());
                    case "Nozzle" -> value.setCellValue(datosTmp.getNozzle());
                    case "Gas Pressure" -> value.setCellValue(datosTmp.getGas_pressure());
                    case "Nozzle Gap" -> value.setCellValue(datosTmp.getNozzle_gap());
                    case "Process time" -> value.setCellValue(datosTmp.getProcess_time());
                    default -> {
                    }

                }
            }
            Costs costsTmp = new Costs();
            //Lo de abajo muestra los datos, verificar que el valor se ha modificado y los calculos son correctos
            for (int l = 30; l<34; l++){
                Row rowInsert = sheet.getRow(l);
                Cell valueName = rowInsert.getCell(1);
                Cell value = rowInsert.getCell(4);
                if(value == null) {
                    value = rowInsert.createCell(4);
                }
                FormulaEvaluator evaluator = wb.getCreationHelper().createFormulaEvaluator();
                CellValue cellValue = evaluator.evaluate(value);
                switch (valueName.getStringCellValue()){
                    case "Process time" -> costsTmp.setTime(datosTmp.getProcess_time());
                    case "Electrical cost" -> costsTmp.setElectrical_costs(cellValue.getNumberValue());
                    case "Assist Gas cost" -> costsTmp.setGas_cost(cellValue.getNumberValue());
                    case "Total" -> costsTmp.setTotal(cellValue.getNumberValue());
                    default -> {}
                }
            }
            costsTmp.setName(datosTmp.getNombre_material_laser());
            costsList.add(costsTmp);
            fos = new FileOutputStream(files.get(0).toAbsolutePath().toString());
            wb.write(fos);
        }


        fis.close();
        wb.close();
        fos.close();

        return costsList;
    }
    @FXML
    private void switchToSecondary() throws IOException {
        App.setRoot("secondary");
    }
}
