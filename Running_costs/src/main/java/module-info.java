module org.openjfx {
    requires javafx.controls;
    requires javafx.fxml;
    requires com.fasterxml.jackson.dataformat.xml;
    requires com.opencsv;
    requires org.apache.poi.poi;
    requires org.apache.poi.ooxml;
    requires com.fasterxml.jackson.databind;
    requires java.sql;
    opens org.openjfx to javafx.fxml;
    opens org.openjfx.DatosCSV;
    opens org.openjfx.XML;
    opens org.openjfx.Resultados;
    exports org.openjfx;
}
