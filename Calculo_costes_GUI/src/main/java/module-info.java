module com.example {
    requires com.fasterxml.jackson.dataformat.xml;
    requires com.opencsv;
    requires javafx.fxml;
    requires javafx.controls;
    requires com.fasterxml.jackson.databind;

    requires poi;
    requires poi.ooxml;
    requires commons.math3;
    opens com.example.calculo_costes_gui;
    exports com.example.calculo_costes_gui;
    //opens com.example.calculo_costes_gui to javafx.base, javafx.controls, javafx.fxml;
}