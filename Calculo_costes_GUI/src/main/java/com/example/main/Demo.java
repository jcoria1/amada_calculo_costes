package com.example.main;

import com.example.DatosCSV.DatosCSV;
import com.example.Resultados.Costs;
import com.example.Resultados.Datos;
import com.example.XML.ArrayOfVLaserMaterial;
import com.example.XML.CutProcessCond;
import com.example.XML.ProcessC;
import com.example.XML.vLaserMaterial;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.dataformat.xml.XmlMapper;
import com.opencsv.CSVReader;
import com.opencsv.bean.CsvToBeanBuilder;
import org.apache.poi.ss.usermodel.*;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.*;
import java.util.stream.Collectors;

public class Demo {
    public static final String processCName = "E005";
    public static FileInputStream fis;
    public static FileOutputStream fos;

    static Scanner scanner = new Scanner(System.in);


    public static void main(String[] args) throws Exception {

        System.out.println("Bienvenido al Software de cálculo de costes para máquinas Láser de AMADA\n" +
                "Al ser una demo se va a mostrar únicamente en formato CLI\n" +
                "Por favor, introduce la ruta del archivo CSV obtenido del nesting en el siguiente formato:\nC:\\ruta\\al\\archivo.csv");
        //INICIO PRUEBAS CSV
        String ruta = scanner.nextLine();
        ruta.replace("\\", "\\\\");
        if (ruta.toUpperCase(Locale.ROOT).endsWith(".CSV")){
            List<Costs> costes = setData(ruta);
            int i = 1;
            for (Costs costsTmp: costes){
                System.out.println(costsTmp.toString());
                i++;
            }
        }else{System.out.println("El programa únicamente soporta archivos con extensión .CSV");}
        System.out.println("Pulsa ENTER para cerrar el programa...");
        scanner.nextLine();
        scanner.close();


    }
    public static List<Datos> getData(String ruta) throws Exception {
        List<Datos> datosList = new ArrayList<>();
        List<DatosCSV> datosCSV = getCSV(ruta);
        for (DatosCSV datosTmp: datosCSV) {
            XmlMapper xmlMapper = (XmlMapper) new XmlMapper()
                    .configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false)
                    .enable(SerializationFeature.INDENT_OUTPUT);
            //Acabar el Path del archivo cutting_condition.xml

            String path = "C:\\ProgramData\\AMADA3i\\ParameterExplorer_Base\\server\\Machines\\Blank\\"+datosTmp.getNombre_maquina()+"\\CUTTING_CONDITION.xml";
            String readContent = new String(Files.readAllBytes(Paths.get(path)));
            ArrayOfVLaserMaterial example = xmlMapper.readValue(readContent, ArrayOfVLaserMaterial.class);
            vLaserMaterial vLaserMaterial = null;
            for (vLaserMaterial vLaserMaterialTmp : example.getvLaserMaterialEntries()) {
                if (vLaserMaterialTmp.getLaserMaterialName().equals(datosTmp.getNombre_material_laser())) {
                    vLaserMaterial = vLaserMaterialTmp;
                    break;
                }
            }
            //Get ProcessC
            CutProcessCond example2 = vLaserMaterial.getCutConditionXmlElement().getCutCond().getCutProcessCond();
            ProcessC processC = null;
            for (ProcessC processTmp : example2.getProcessCList()) {
                if (processTmp.getName().equals(processCName)) {
                    processC = processTmp;
                    break;
                }
            }


            //Obtenemos Datos
            Datos datoInsert = new Datos();
            //set NAME
            String[] nombre = datosTmp.getNombre_maquina().split("-");
            datoInsert.setName(nombre[0]);
            //set Nombre_material_laser
            datoInsert.setNombre_material_laser(datosTmp.getNombre());
            //set TIMEl
            datoInsert.setProcess_time(datosTmp.getTiempo());
            //get/set Nozzle
            datoInsert.setNozzle(vLaserMaterial.getCutConditionXmlElement().getCutCond().getNozzle());

            //get/set Speed
            datoInsert.setSpeed(processC.getSpeed());

            //get/set Output
            datoInsert.setPower(processC.getOutput());

            //get/set Duty
            datoInsert.setDuty(processC.getDuty());

            //get/set kind
            datoInsert.setAssist_gas(processC.getKind());

            //get/set pressure
            datoInsert.setGas_pressure(processC.getPressure());

            //get/set nozzleGap (origionRepos)
            datoInsert.setNozzle_gap(processC.getOrigionRepos());;

            //Añadimos el objeto en la lista de datos para trabajar en él
            datosList.add(datoInsert);
        }
        /*

        RETURN LIST OF DATA

        */
        return datosList;
    }
    public static List getCSV(String ruta) throws IOException {
        //TESTS
        CSVReader csvReader = new CSVReader(new FileReader(ruta));
        List<DatosCSV> beans = new CsvToBeanBuilder(csvReader)
                .withType(DatosCSV.class).build().parse();
        /*Map<String, List<DatosCSV>> datos = new HashMap<>();
        for (DatosCSV datosTmp:
             beans) {
            if (datosTmp.getTipo().equals("CHAPA")){

            }else{

            }
        }*/
        beans.removeIf(datosCSV -> datosCSV.getTipo().equals("PIEZA"));

        return beans;
        //FIN TESTS
    }

    //NEW setData()
    public static List<Costs> setData(String ruta) throws Exception {

        String filepath = "C:\\Amada Config\\costes";

        List<Datos> datosList = getData(ruta);
        Workbook wb = null;


        List<Costs> costsList= new ArrayList<>();
        for (Datos datosTmp : datosList){

            List<Path> files = Files.walk(Path.of(filepath))
                    .filter(Files::isRegularFile)
                    .filter(s -> s.getFileName().toString().startsWith(datosTmp.getName()+"-"+datosTmp.getPower()+"-"))
                    .collect(Collectors.toList());
            fis = new FileInputStream(files.get(0).toAbsolutePath().toString());
            wb = new XSSFWorkbook(fis);
            Sheet sheet = wb.getSheet("Cost simulation"); //Leemos los valores que nos hemos encontrado en la tabla antes de modificarlos y los mostramos por pantalla
            // Ahora vamos a insertar datos de ejemplo en la hoja de cálculo, para después obtener los valores que nos interesan y mostrarlos por pantalla
            for (int k = 16; k<31; k++){
                Row rowInsert = sheet.getRow(k);
                Cell valueName = rowInsert.getCell(1);
                Cell value = rowInsert.getCell(4);
                if(value == null) {
                    value = rowInsert.createCell(4);
                }
                switch (valueName.getStringCellValue()){
                    case "Cutting Speed" -> value.setCellValue(datosTmp.getSpeed());
                    case "Power" -> value.setCellValue(datosTmp.getPower());
                    case "Duty" -> value.setCellValue(datosTmp.getDuty());
                    case "Assist gas" -> value.setCellValue(datosTmp.getAssist_gas());
                    case "Nozzle" -> value.setCellValue(datosTmp.getNozzle());
                    case "Gas Pressure" -> value.setCellValue(datosTmp.getGas_pressure());
                    case "Nozzle Gap" -> value.setCellValue(datosTmp.getNozzle_gap());
                    case "Process time" -> value.setCellValue(datosTmp.getProcess_time());
                    default -> {
                    }

                }
            }
            Costs costsTmp = new Costs();
            //Lo de abajo muestra los datos, verificar que el valor se ha modificado y los calculos son correctos
            for (int l = 30; l<34; l++){
                Row rowInsert = sheet.getRow(l);
                Cell valueName = rowInsert.getCell(1);
                Cell value = rowInsert.getCell(4);
                if(value == null) {
                    value = rowInsert.createCell(4);
                }
                FormulaEvaluator evaluator = wb.getCreationHelper().createFormulaEvaluator();
                CellValue cellValue = evaluator.evaluate(value);
                switch (valueName.getStringCellValue()){
                    case "Process time" -> costsTmp.setTime(datosTmp.getProcess_time());
                    case "Electrical cost" -> costsTmp.setElectrical_costs(cellValue.getNumberValue());
                    case "Assist Gas cost" -> costsTmp.setGas_cost(cellValue.getNumberValue());
                    case "Total" -> costsTmp.setTotal(cellValue.getNumberValue());
                    default -> {}
                }
            }
            costsTmp.setName(datosTmp.getNombre_material_laser());
            costsList.add(costsTmp);
            fos = new FileOutputStream(files.get(0).toAbsolutePath().toString());
            wb.write(fos);
        }


        fis.close();
        wb.close();
        fos.close();

        return costsList;
    }
}
