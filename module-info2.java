module com.example.calculo_costes_gui {
    requires javafx.controls;
    requires javafx.fxml;
    requires javafx.web;

    requires org.controlsfx.controls;
    requires com.dlsc.formsfx;
    requires org.kordamp.ikonli.javafx;
    requires org.kordamp.bootstrapfx.core;
    requires eu.hansolo.tilesfx;
    requires com.almasb.fxgl.all;
    requires com.opencsv;
    requires com.fasterxml.jackson.databind;
    requires com.fasterxml.jackson.dataformat.xml;
    requires java.sql;
    requires org.apache.poi.ooxml;


    //opens com.example.DatosCSV to javafx.fxml, javafx.base;
    opens com.example.Resultados to javafx.fxml, javafx.base, com.fasterxml.jackson.databind;
    //opens com.example.XML to javafx.fxml, javafx.base;
    opens com.example.calculo_costes_gui to javafx.fxml, javafx.base;
    opens com.example.DatosCSV to com.fasterxml.jackson.databind;
    opens com.example.XML to com.fasterxml.jackson.databind;


}